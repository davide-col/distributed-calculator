#ifndef ORCHESTRATEUR_H
#define ORCHESTRATEUR_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>
#include <time.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <math.h>

// Structure de la mémoire partagée
typedef struct shared {
  char * buf;
} * Shared;

// Structure d'un calcul
typedef struct calcul {
  char operateur; // Opérateur
  float x; // Valeur x
  float y; // Valeur y
  int exists; // Boolean à 1 si calcul non traité et 0 sinon
} * Calcul;

// Structure d'un Noeud de la liste chaînée
typedef struct node {
  char operateur; // Opérateur/Fonction du Noeud
  struct node * next; // Pointeur vers le prochain Noeud
  struct sockaddr_storage addr; // Adresse du Noeud
  int timer_hello; // Délai écoulé en secondes du dernier paquet "hello" reçu
  int timer_cal; // Délai écoulé en secondes aprés l'envoi du calcul
  int occupied; // Test si Noeud est occupé
  char * read_time; // Date de la demande du calcul
  in_port_t port; // Numéro de port du Noeud
  char * ip_str; // IP du Noeud
  Calcul cal; // Calcul envoyé au Noeud (sruct calcul)
  char * cal_str; // Calcul envoyé au Noeud (char *)
} * Node;

// Retour du numéro de port
in_port_t get_in_port (struct sockaddr *);

// Suppression des sauts de lignes dans la chaîne de caractères
// Retourne la chaine de caractère modifiée
char * delete_jumpline (char *);

// Retourne l'adresse IP en chaîne de caractères
char * get_ip_string(struct sockaddr_storage);

// Insertion d'un noeud dans la liste chaînée
// Retourne -1 si le noeud est déjà listé et 0 sinon
int insert_node (Node);

// Recherche du Noeud dans la liste par comparaison d'adresses
// Return : NULL si aucun Noeud est disponible et retourne un Noeud sinon
Node search_node (struct sockaddr_storage);

// Recherche d'un noeud disponible dans la liste par son operateur
// Return : NULL si aucun Noeud est disponible et Node sinon
Node search_operational_node (char);

// Suppression d'un noeud dans la liste chaînée
// Retourne -1 si échec et 0 sinon
int delete_node (Node);

// Fonction de récupération d'un calcul qui recherche un autre Noeud compatible
void resend_cal (Calcul, char *, char *);

// Fonction qui contrôle le temps avec incrémentation des compteurs du temps de calcul
// Et le compteur du dernier paquet "hello" reçu
// Si les compteurs atteindent le temps timeout, appel de la fonction resend_cal qui renvoie
// le calcul à un Noeud adaptée
void timer_control ();

// Remet le timer à 0 pour le noeud correspondant
// Retourne -1 si échec et 0 sinon
int reset_timer (struct sockaddr_storage);

// Conversion du calcul en struct calcul
// Stoque le calcul dans la variable Calcul cal
// Retourne -1 si le calcul est non conforme et 0 sinon
int parser_calcul (char *, Calcul);

// Création de l'adressage de l'orchestrateur
void create_orch_socket ();

//Fonction de création d'un calcul
//Retourne : structu calcul
Calcul create_cal ();

// Création d'un Noeud
Node create_node (struct sockaddr_storage, char);

// Affichage des Noeuds dans la liste chaînée
void show_nodes ();

// Test si le bufer contient "exit"
// Retourne 1 en cas de succès et 0 sinon
int is_exit(char *);

// Test si le bufer contient "show"
// Retourne 1 en cas de succès et 0 sinon
int is_show(char *);

// Test si le bufer contient "connect"
// Retourne 1 en cas de succès et 0 sinon
int is_connect(char *);

// Test si le bufer contient "hello"
// Retourne 1 en cas de succès et 0 sinon
int is_hello(char *);

// Fonction du thread qui transmet les entrées tapées dans le terminal (stdin)
void * thread_terminal_input ();

// Création du thread qui gère les différents timers
void * thread_timer_control ();

// Création du thread qui transmet les entrées tapées dans le terminal (stdin)
void create_thread_terminal();

// Création du thread qui transmet les entrées tapées dans le terminal (stdin)
void create_thread_timer();

// Fonction qui vérifie les arguuments du main
// Si échec exit du processus
void verify_arguments(int, char **);

//Libère la mémoire d'une chaine de caractères
void free_string(char *);

// Copie le calcul du deuxième argument dans le premier
void copy_cal (Calcul,Calcul);

// Supprimme un calcul en le rendant obselète
// exists est mis à 0
void delete_cal(Calcul);

// Libère la mémoire de la mémoire partagée enter les thread
void free_shared_mem ();

// Suppression de tous les noeuds de la liste
void delete_all_nodes ();

// Retourne la taille de l'addressage entrée en argument
socklen_t sockaddr_length(struct sockaddr_storage);

#endif
