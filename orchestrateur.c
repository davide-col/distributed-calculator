#include "orchestrateur.h"
#define TIMER_LOST_CON 35 // Limite du délai (sec) du dernier "hello" reçu avant déconnexion du Noeud
#define TIMER_LOST_CALC 5 // Limite du délai (sec) de la réception du résultat avant redirection du calcul
#define ORCH_PORT 5000 // Port de l'Orchestrateur

int sockfd; // Socket de l'Orchestrateur
socklen_t my_addrlen; // Taille de l'adressage de l'Orchestrateur
struct sockaddr_storage my_addr; // Adresse de l'Orchestrateur

int flag_stop = 0; // Fin du processus
int flag_debug = 0; // Mode debug

pthread_t pth[2]; // Identifiants des deux threads
Shared shared_mem; // "Mémoire partagée"
Node list_nodes; // Liste des noeuds

// Mutex pour la concurrence des processus
pthread_mutex_t list_mutex;
pthread_mutexattr_t attr_mutex;

// Retour du numéro de port
in_port_t get_in_port (struct sockaddr *sa){
    if (sa->sa_family == AF_INET) {
        return (((struct sockaddr_in*)sa)->sin_port);
    }
    return (((struct sockaddr_in6*)sa)->sin6_port);
}

// Suppression des sauts de lignes dans la chaîne de caractères
// Retourne la chaine de caractère modifiée
char * delete_jumpline (char * str){
  int i;
  for (i=0;i<strlen(str);i++){
    if (str[i]=='\n') str[i]=' ';
  }
  return str;
}

// Retourne l'adresse IP en chaîne de caractères
char * get_ip_string (struct sockaddr_storage addr){
  char *s = NULL;
  switch(addr.ss_family) {
    case AF_INET: {
        struct sockaddr_in * addr_in = (struct sockaddr_in *)&addr;
        s = malloc(INET_ADDRSTRLEN);
        inet_ntop(AF_INET, &(addr_in->sin_addr), s, INET_ADDRSTRLEN);
        break;
    }
    case AF_INET6: {
        struct sockaddr_in6 * addr_in6 = (struct sockaddr_in6 *)&addr;
        s = malloc(INET6_ADDRSTRLEN);
        inet_ntop(AF_INET6, &(addr_in6->sin6_addr), s, INET6_ADDRSTRLEN);
        break;
    }
    default:
        break;
  }
  delete_jumpline(s);
  return s;
}

// Insertion d'un noeud dans la liste chaînée
// Retourne -1 si le noeud est déjà listé et 0 sinon
int insert_node (Node node){
  pthread_mutex_lock(&list_mutex);
  Node tmp = list_nodes;
  if (list_nodes==NULL){
    list_nodes=node;
    pthread_mutex_unlock(&list_mutex);
    return 0;
  }
  // Parcours de tous le Noeuds avec Test s'il existe
  while (tmp->next!=NULL){
    if((tmp->port==node->port)&&(strcmp(tmp->ip_str,node->ip_str)==0)){
      pthread_mutex_unlock(&list_mutex);
      return -1;
    }
    tmp=tmp->next;
  }
  // Test pour le dernière valeur
  if((tmp->port==node->port)&&(strcmp(tmp->ip_str,node->ip_str)==0)){
    pthread_mutex_unlock(&list_mutex);
    return -1;
  }
  // Insertion du nouveau noeud dans la liste
  tmp->next=node;
  pthread_mutex_unlock(&list_mutex);
  return 0;
}

// Recherche du Noeud dans la liste par comparaison d'adresses
// Return : NULL si aucun Noeud est disponible et retourne un Noeud sinon
Node search_node (struct sockaddr_storage addr){
  char * ip_str=NULL;
  pthread_mutex_lock(&list_mutex);
  in_port_t port;
  if (addr.ss_family==AF_INET){
    port=((struct sockaddr_in *) &addr)->sin_port;
  }
  else {
    port=((struct sockaddr_in6 *) &addr)->sin6_port;
  }
  Node tmp = list_nodes;
  if(list_nodes==NULL){ // Si la liste est vide, on retourne NULL
    pthread_mutex_unlock(&list_mutex);
    return NULL;
  }
  // Parcours de la liste de Noeuds
  while ((tmp->next!=NULL)&&!((tmp->port==port)&&(strcmp(tmp->ip_str,ip_str=get_ip_string(addr))==0))){
    free_string(ip_str);
    ip_str=NULL;
    tmp=tmp->next;
  }
  if ((tmp->port==port)&&(strcmp(tmp->ip_str,ip_str=get_ip_string(addr))==0)){ // Si trouvé on retourne le Noeud
    free_string(ip_str);
    ip_str=NULL;
    pthread_mutex_unlock(&list_mutex);
    return tmp;
  }
  else { // Sinon on retourne NULL
    free_string(ip_str);
    ip_str=NULL;
    pthread_mutex_unlock(&list_mutex);
    return NULL;
  }
}

// Recherche d'un noeud disponible dans la liste par son operateur
// Return : NULL si aucun Noeud est disponible et Node sinon
Node search_operational_node (char operateur){
  pthread_mutex_lock(&list_mutex);
  Node tmp = list_nodes;
  if(list_nodes==NULL){ // Si la liste est vide
    pthread_mutex_unlock(&list_mutex);
    return NULL;
  }
  while (tmp->next!=NULL){ // Parcours de la liste de Noeuds
    if ((tmp->operateur==operateur)&&(tmp->occupied==0)){
      pthread_mutex_unlock(&list_mutex);
      return tmp;
    }
    tmp=tmp->next;
  }
  if ((tmp->operateur==operateur)&&(tmp->occupied==0)){ // Si Noeud trouvé on le retourne
    pthread_mutex_unlock(&list_mutex);
    return tmp;
  }
  else { // Sinon on retourne NULL
    pthread_mutex_unlock(&list_mutex);
    return NULL;
  }
}

// Suppression d'un noeud dans la liste chaînée
// Retourne -1 si échec et 0 sinon
int delete_node (Node node){
  pthread_mutex_lock(&list_mutex);
  Node tmp = list_nodes;
  Node prec = tmp;
  if(list_nodes==NULL){
    pthread_mutex_unlock(&list_mutex);
    return -1;
  }
  if(list_nodes==node){ // Si premier élément de la liste suppression
    list_nodes=list_nodes->next;
    free_string(tmp->read_time);
    free_string(tmp->ip_str);
    free_string(tmp->cal_str);
    free(tmp->cal);
    free(tmp);
    tmp=NULL;
    pthread_mutex_unlock(&list_mutex);
    return 0;
  }
  while ((tmp->next!=NULL)&&(tmp!=node)){ // Parcours de la liste
    prec=tmp;
    tmp=tmp->next;
  }
  if (tmp==node){ // Test et suppression
    prec->next=tmp->next;
    free_string(tmp->read_time);
    free_string(tmp->ip_str);
    free_string(tmp->cal_str);
    free(tmp->cal);
    free(tmp);
    tmp=NULL;
    pthread_mutex_unlock(&list_mutex);
    return 0;
  }
  // Retourbe -1 si pas trouvé
  pthread_mutex_unlock(&list_mutex);
  return -1;
}

// Suppression de tous les noeuds de la liste
void delete_all_nodes (){
  Node tmp = list_nodes;
  Node prec = NULL;
  if (list_nodes==NULL){ // Si la liste de Noeuds est vide
    return;
  }
  while (tmp->next!=NULL){ // Parcours de la liste de Noeuds
    prec=tmp;
    tmp=tmp->next;
    delete_node(prec); // Suppression
  }
  delete_node(tmp); // Suppression
}

// Copie le calcul du deuxième argument dans le premier
void copy_cal (Calcul c1, Calcul c2){
  c1->operateur=c2->operateur;
  c1->x=c2->x;
  c1->y=c2->y;
  c1->exists=c2->exists;
}

// Fonction de récupération d'un calcul qui recherche un autre Noeud compatible
void resend_cal (Calcul cal, char * read_time, char * cal_str){
  char * ip_str = NULL;
  printf("\nRecherche d'un autre Noeud pour : %s\norchestrateur> ",cal_str);
  fflush(stdout);
  Node tmp;
  char buf[2014];
  // Extraction d'un Noeud opérationnel
  tmp=search_operational_node(cal->operateur);
  if(tmp==NULL){ // Aucun Noeud trouvé pour le calcul
    printf("\nAucun noeud disponible pour %c\norchestrateur> ",cal->operateur);
    fflush(stdout);
  }
  else{ // Envoi du calcul
    printf("\nNoeud opérationnel trouvé <%s,%d>\norchestrateur> ",ip_str=get_ip_string(tmp->addr),tmp->port);
    free_string(ip_str);
    ip_str=NULL;
    fflush(stdout);
    // Préparation du Noeud
    pthread_mutex_lock(&list_mutex);
    tmp->occupied = 1;
    copy_cal(tmp->cal,cal);
    delete_cal(cal);
    strcpy(tmp->read_time,read_time);
    strcpy(tmp->cal_str,cal_str);
    strcpy(buf,cal_str);
    pthread_mutex_unlock(&list_mutex);
    // ----------
    // Envoi du calcul au Noeud
    if (sendto(sockfd, buf, sizeof(buf) + 1, 0, (struct sockaddr *)&tmp->addr, sockaddr_length(tmp->addr)) == -1){
      perror("Orchestrateur error (sendto)");
      close(sockfd);
      exit(EXIT_FAILURE);
    }
  }
}

// Fonction qui contrôle le temps avec incrémentation des compteurs du temps de calcul
// Et le compteur du dernier paquet "hello" reçu
// Si les compteurs atteindent le temps timeout, appel de la fonction resend_cal qui renvoie
// le calcul à un Noeud adaptée
void timer_control (){
  char * ip_str = NULL;
  pthread_mutex_lock(&list_mutex);
  Node tmp1 = list_nodes;
  Node tmp2;
  if(list_nodes==NULL){ // Si la liste est vide on sors
    pthread_mutex_unlock(&list_mutex);
    return;
  }
  while (tmp1->next!=NULL){
    // Parcours de chaque Noeud dans la liste
    if (tmp1->cal->exists) tmp1->timer_cal++;
    tmp1->timer_hello++;

    if (tmp1->timer_hello>=TIMER_LOST_CON){ // Perte de connexion avec le Noeud
      printf("\nConnection perdue avec <%s,%d>\norchestrateur> ",ip_str=get_ip_string(tmp1->addr),get_in_port((struct sockaddr*)&tmp1->addr));
      fflush(stdout);
      free_string(ip_str);
      ip_str=NULL;
      if (tmp1->cal->exists){
        resend_cal(tmp1->cal,tmp1->read_time,tmp1->cal_str);
      }
      tmp2=tmp1;
      tmp1=tmp1->next;
      delete_node(tmp2);
      continue;
    }
    else if (tmp1->timer_cal>TIMER_LOST_CALC){ // Perte du calcul/résultat avec le Noeud, suppression du Noeud
      if (tmp1->cal->exists){
        printf("\nRésultat de %sà <%s,%d> perdu\norchestrateur> ",tmp1->cal_str,ip_str=get_ip_string(tmp1->addr),get_in_port((struct sockaddr*)&tmp1->addr));
        fflush(stdout);
        free_string(ip_str);
        ip_str=NULL;
        resend_cal(tmp1->cal,tmp1->read_time,tmp1->cal_str);
      }
      // Suppression du Noeud considéré comme déconnecté
      tmp2=tmp1;
      tmp1=tmp1->next;
      delete_node(tmp2);
      continue;
    }
    tmp1=tmp1->next;
  }
  // Traitement du dernier Noeud de la liste
  tmp1->timer_hello++;
  if (tmp1->cal->exists) tmp1->timer_cal++;

  if (tmp1->timer_hello>=TIMER_LOST_CON){ // Perte de connexion avec le Noeud
    printf("\nConnection perdue avec <%s,%d>\norchestrateur> ",ip_str=get_ip_string(tmp1->addr),get_in_port((struct sockaddr*)&tmp1->addr));
    fflush(stdout);
    free_string(ip_str);
    ip_str=NULL;
    if (tmp1->cal->exists){
      resend_cal(tmp1->cal,tmp1->read_time,tmp1->cal_str);
      tmp1->cal_str=NULL;
    }
    delete_node(tmp1);
  }
  else if (tmp1->timer_cal>TIMER_LOST_CALC){ // Perte du calcul/résultat avec le Noeud, suppression du Noeud
    if (tmp1->cal->exists){
      printf("\nRésultat de %s à <%s,%d> perdu timer_cal : %d\norchestrateur> ",tmp1->cal_str,ip_str=get_ip_string(tmp1->addr),get_in_port((struct sockaddr*)&tmp1->addr),tmp1->timer_cal);
      fflush(stdout);
      free_string(ip_str);
      ip_str=NULL;
      resend_cal(tmp1->cal,tmp1->read_time,tmp1->cal_str);
      tmp1->cal_str=NULL;
    }
    // Suppression du Noeud considéré comme déconnecté
    delete_node(tmp1);
  }
  pthread_mutex_unlock(&list_mutex);
}

// Remet le timer à 0 pour le noeud correspondant
// Retourne -1 si échec et 0 sinon
int reset_timer (struct sockaddr_storage addr){
  pthread_mutex_lock(&list_mutex);
  Node tmp=search_node(addr);
  if(tmp!=NULL){
    tmp->timer_hello=0;
  }
  else{
    pthread_mutex_unlock(&list_mutex);
    return -1; // Le noeud n'est pas stocké
  }
  pthread_mutex_unlock(&list_mutex);
  return 0;
}

// Conversion du calcul en struct calcul
// Stoque le calcul dans la variable Calcul cal
// Retourne -1 si le calcul est non conforme et 0 sinon
int parser_calcul (char * string, Calcul cal){
  // Réinitialisation de la variable cal
  cal->operateur='\0';
  cal->x=0;
  cal->y=0;
  int xdec_pos          = 0; // Curseur de lecture de la partie décimale de x
  int ydec_pos          = 0; // Curseur de lecture de la partie décimale de y
  int indice            = 0; // Curseur de lecture du buffer string
  int etape             = 0; // Indice pour départager les différentes parties du string
  float tmp_val         = 0; // Variable temporelle pour les calculs
  int flag_x            = 0; // Test si x existe
  int flag_y            = 0; // Test si y existe
  int nb_parentheses    = 0; // Vérification des parentheses
  int flag_non_conforme = 0; // Vérification de la validité du calcul
  int flag_x_neg        = 0; // Si x est négatif
  int flag_y_neg        = 0; // Si y est négatif
  while ((indice < strlen(string))&&(etape < 3)){
    if (etape == 0){ // Lecture de l'opérateur
      cal->operateur = string[indice];
      etape ++;
      indice ++;
    }
    if (etape == 1){ // Lecture de x
      if(!isdigit(string[indice])){ // Test si c'est un chiffre
        if (string[indice]=='('){
          nb_parentheses++;
          indice ++;
          if(string[indice]=='-'){ // Si ',' lecture de y
            flag_x_neg=1;
            indice++;
          }
        }
        else if(string[indice]=='.'){ // Si '.' lecture de la partie decimale de x
          xdec_pos=1;
          indice++;
        }
        else if(string[indice]==','){ // Si ',' lecture de y
          etape++;
          indice++;
          if(string[indice]=='-'){ // Si ',' lecture de y
            flag_y_neg=1;
            indice++;
          }
        }
        else{
          flag_non_conforme = 1;
          break;
        }
      }
      else{
        if(xdec_pos>0){ // Partie décimale de x
          tmp_val=string[indice]-'0';
          cal->x=cal->x + (tmp_val/(pow(10,xdec_pos)));
          xdec_pos++;
          indice++;
        }
        else { // Partie entière de x
        flag_x=1;
        cal->x *= 10;
        cal->x += string[indice]-'0';
        indice++;
        }
      }
    }
    if (etape == 2){ // Lecture de y
      if(!isdigit(string[indice])){ // Test si c'est un chiffre
        if(string[indice]=='.'){ // Si '.' lecture de la partie decimale de y
          ydec_pos=1;
          indice++;
        }
        else if(string[indice]==')'){ // Si ')' fin de la lecture
          nb_parentheses++;
          etape++;
        }
        else{
          flag_non_conforme = 1;
          break;
        }
      }
      else{
        if(ydec_pos>0){ // Partie décimale de y
          tmp_val=string[indice]-'0';
          cal->y=cal->y + (tmp_val/(pow(10,ydec_pos)));
          ydec_pos++;
          indice++;
        }
        else { // Partie entière de y
        flag_y=1;
        cal->y *= 10;
        cal->y += string[indice]-'0';
        indice++;
        }
      }
    }
  }
  if (flag_x_neg) cal->x = -cal->x;
  if (flag_y_neg) cal->y = -cal->y;
  // Test de la validité du calcul
  if ((!flag_x)||(!flag_y)||(nb_parentheses!=2)||(flag_non_conforme)){
    printf("Calcul non conforme. Ex : +(3,7)\n");
    return -1; // Echec
  }
  // Test de la validité de l'opérateur
  if ((cal->operateur!='+')&&(cal->operateur!='-')&&(cal->operateur!='*')&&(cal->operateur!='/')){
    printf("Calcul non conforme. : opérateur non valide, veuillez choisir entre '+','-','/','*'\n");
    return -1; // Echec
  }
  // Test de la division par 0
  if ((cal->operateur=='/')&&(cal->y==0.)){
    printf("Calcul non conforme. : division par 0\n");
    return -1; // Echec
  }
  return 0; // Calcul avec succés
}

// Création de l'adressage de l'Orchestrateur
void create_orch_socket (){
  // Création du socket de l'Orchestrateur
  sockfd = socket (AF_INET6, SOCK_DGRAM, 0);
  if (sockfd == -1){
      perror ("error (socket)");
      exit(1);
  }
  if(flag_debug){ // Affichage avec l'option debug
    printf("\nCréation Socket IPv4 et IPv6\norchestrateur> ");
    fflush(stdout);
  }
  // Création d'adressage de l'Orchestrateur IPv6
  // Initialisation de la structure d'adressage
  struct sockaddr_in6 my_addr_tmp;
  memset (&my_addr_tmp, 0, sizeof(my_addr_tmp));
  my_addr_tmp.sin6_family   = AF_INET6;
  my_addr_tmp.sin6_port     = htons (ORCH_PORT);
  my_addrlen                = sizeof(struct sockaddr_in6);
  my_addr_tmp.sin6_addr	    = in6addr_any;
  // Jumelage de l'adresse avec le socket
  if(bind(sockfd, (void *) &my_addr_tmp, my_addrlen) == -1){
      perror("error (bind)");
      close(sockfd);
      exit(EXIT_FAILURE);
  }
  // Copie dans la variable my_addr
  memcpy((char *)(&my_addr),(char *)(&my_addr_tmp), sizeof(my_addr_tmp));
}

//Fonction de création d'un calcul
//Retourne : structu calcul
Calcul create_cal (){
  Calcul cal = malloc (sizeof (struct calcul));
  cal->x=0;
  cal->y=0;
  cal->operateur='\0';
  cal->exists=0;
  return cal;
}

// Supprimme un calcul en le rendant obselète
// exists est mis à 0
void delete_cal (Calcul cal){
  cal->exists=0;
}

// Création d'un Noeud
Node create_node (struct sockaddr_storage addr, char operateur){
  Node node = malloc (sizeof(struct node));
  node->operateur=operateur;
  node->occupied=0;
  node->next=NULL;
  node->timer_hello=0;
  node->timer_cal=0;
  node->addr=addr;
  node->cal=create_cal();
  node->read_time=malloc(sizeof(char)*256);
  node->port=get_in_port(((struct sockaddr*)&addr));
  node->cal_str=malloc(sizeof(char)*1024);
  node->ip_str=get_ip_string(addr);
  return node;
}

// Affichage des Noeuds dans la liste chaînée
void show_nodes (){
  Node tmp = list_nodes;
  char * ip_str=NULL;
  if(list_nodes==NULL){ // Retour si vide
    printf("Aucun Noeud connecté\n");
    return;
  }
  // Parcours de la liste
  while (tmp->next!=NULL){
    printf("Node %c -> <%s,%d> timer_hello %ds, timer_cal %ds, occupied %d \n",tmp->operateur,ip_str=get_ip_string(tmp->addr),tmp->port,tmp->timer_hello,tmp->timer_cal,tmp->occupied);
    free_string(ip_str);
    ip_str=NULL;
    tmp=tmp->next;
  }
  // Affichage du dernier élément de la liste
  printf("Node %c -> <%s,%d> timer_hello %ds, timer_cal %ds, occupied %d\n",tmp->operateur,ip_str=get_ip_string(tmp->addr),tmp->port,tmp->timer_hello,tmp->timer_cal,tmp->occupied);
  free_string(ip_str);
  ip_str=NULL;
}

// Test si le bufer contient "exit"
// Retourne 1 en cas de succès et 0 sinon
int is_exit(char * string){
  return ((string[0]=='e') && (string[1]=='x') && (string[2]=='i') && (string[3]=='t'));
}

// Test si le bufer contient "show"
// Retourne 1 en cas de succès et 0 sinon
int is_show(char * string){
  return ((string[0]=='s')&&(string[1]=='h')&&(string[2]=='o')&&(string[3]=='w'));
}

// Test si le bufer contient "connect"
// Retourne 1 en cas de succès et 0 sinon
int is_connect(char * string){
  return ((string[0]=='c')&&(string[1]=='o')&&(string[2]=='n')&&(string[3]=='n')&&(string[4]=='e')&&(string[5]=='c')&&(string[6]=='t'));
}

// Test si le bufer contient "hello"
// Retourne 1 en cas de succès et 0 sinon
int is_hello(char * string){
  return ((string[0]=='h')&&(string[1]=='e')&&(string[2]=='l')&&(string[3]=='l')&&(string[4]=='o'));
}

// Fonction du thread qui transmet les entrées tapées dans le terminal (stdin)
void * thread_terminal_input (){
  char * ip_str = NULL;
  // Variable de stockage des calculs
  Calcul cal_tmp=cal_tmp=create_cal();
  Node node_tmp2;
  // Variables pour le calcul de temps
  time_t t;
  struct tm tm;
  // ----------
  while(1){
    if(flag_stop) pthread_exit(0);
    fflush(stdout);
    fgets(shared_mem->buf,1024,stdin);
    if (is_exit(shared_mem->buf)){ // S'il contient "exit", arrêt du processus
      flag_stop = 1; // Stop pour le processus (main)
      delete_all_nodes(); // Suppression de tous les noeuds
      free(cal_tmp);
      free(shared_mem->buf);
      free(shared_mem);
      printf("logged out\n");
      close(sockfd); // Sortie de l'attente passive dans le main
      exit(0);
    }
    else if (is_show(shared_mem->buf)){ // S'il contient "show", affichage des Noeuds sauvegardés
      show_nodes();
    }
    else {
      if(parser_calcul(shared_mem->buf,cal_tmp)==-1){ // Test si le buffer contient un calcul
        // Si ce n'est pas un calcul conforme, suppression du buffer et repasse en attente passive
        shared_mem->buf[0]='\0';
        printf("orchestrateur> ");
        fflush(stdout);
        continue;
      }
      // Sinon, recharche d'un Noeud compatible opérationnel
      t = time(NULL);
      tm = *localtime(&t);
      node_tmp2=search_operational_node(cal_tmp->operateur);
      if(node_tmp2==NULL){ // Si aucun Noeud compatible trouvé
        printf("Aucun noeud disponible pour %c\n",cal_tmp->operateur);
        fflush(stdout);
        if(flag_debug){ // Affichage avec l'option debug
          printf("Liste des Noeuds :\n");
          show_nodes();
          fflush(stdout);
        }
      }
      else{ // Si Noeud trouvé, envoi du calcul à un Noeud
        pthread_mutex_lock(&list_mutex);
        node_tmp2->occupied=1;
        copy_cal(node_tmp2->cal,cal_tmp);
        node_tmp2->cal->exists=1;
        strftime(node_tmp2->read_time, sizeof(node_tmp2->read_time),"%H:%M:%S", &tm);
        strcpy(node_tmp2->cal_str,shared_mem->buf);
        delete_jumpline(node_tmp2->cal_str);
        pthread_mutex_unlock(&list_mutex);
        if(flag_debug){ // Affichage avec l'option debug
          printf("Noeud opérationnel trouvé <%s,%d>\n",ip_str=get_ip_string(node_tmp2->addr),node_tmp2->port);
          free_string(ip_str);
          ip_str=NULL;
          fflush(stdout);
        }
        if (sendto(sockfd, shared_mem->buf, 1024, 0, (struct sockaddr *)&node_tmp2->addr, sockaddr_length(node_tmp2->addr)) == -1){
          perror("error (sendto)");
          close(sockfd);
          exit(EXIT_FAILURE);
        }
      }
    }
    shared_mem->buf[0]='\0';
    printf("orchestrateur> ");
    fflush(stdout);
  }
}

// Création du thread qui gère les différents timers
void * thread_timer_control (){
  while(1){
    sleep(1);
    timer_control();
    if(flag_stop) pthread_exit(0);
  }
}

// Création du thread qui transmet les entrées tapées dans le terminal (stdin)
void create_thread_terminal(){
  if(pthread_create(&pth[0], NULL, thread_terminal_input, NULL)!=0){
    perror("phthread_create error");
  }
}

// Création du thread qui transmet les entrées tapées dans le terminal (stdin)
void create_thread_timer(){
  if(pthread_create(&pth[1], NULL, thread_timer_control, NULL)!=0){
    perror("phthread_create error");
  }
}

// Fonction qui vérifie les arguuments du main
// Si échec exit du processus
void verify_arguments(int argc, char ** argv){
  // Récupération des options dans le Main
  int get_opt;
  while ((get_opt = getopt (argc, argv, "d"))!=-1){ // Recherche des options avec getopt
      switch (get_opt){
        case 'd': // Option debug
          flag_debug = 1;
          break;
        case '?':
          fprintf(stderr, "Option -%c inconnue.\n",optopt);
          exit(1);
          break;
        default:
          fprintf (stderr, "error getopt\n");
          exit(1);
      }
  }
  // Test du nombre d'arguments
  if(argc != (1+flag_debug)){
    fprintf(stderr,"Utilisation: %s [-d]\n", argv[0]);
    exit(1);
  }
}

// Libère la mémoire de la mémoire partagée enter les thread
void free_shared_mem (){
  free(shared_mem->buf);
  free(shared_mem);
}

// Libère la mémoire d'une chaine de caractères
void free_string (char * string){
  if (string!=NULL){
    free(string);
  }
}

// Retourne la taille de l'addressage entrée en argument
socklen_t sockaddr_length (struct sockaddr_storage addr){
  if(addr.ss_family == AF_INET) {
    return sizeof(struct sockaddr_in);
  }
  else {
    return sizeof(struct sockaddr_in6);
  }
}

// Fonction main :
// Initialisation des variables -> création des thread
// -> lecture des paquets rentrants jusqu'à la fin du processus
// et envoi des paquets "ok"
int main(int argc, char * argv[]) {
  verify_arguments(argc,argv); // Vérification des arguments
  char buf_recv[1024]; // Buffer de messages reçus
  // Initialisation de la "memoire partagée"
  shared_mem = malloc (sizeof(struct shared));
  shared_mem->buf = malloc (sizeof(char)*1024);
  shared_mem->buf[0]='\0';
  // ----------
  Node node_tmp; // Variable temporelle Noeud
  struct sockaddr_storage addr_client; // Variable de stockage des addresses
  // Initialisation des mutex
  pthread_mutexattr_init(&attr_mutex);
  pthread_mutexattr_settype(&attr_mutex, PTHREAD_MUTEX_RECURSIVE);
  pthread_mutex_init(&list_mutex, &attr_mutex);
  // ----------
  create_thread_terminal(); // Creation d'un thread qui lit l'entrée stdin
  printf("orchestrateur> "); // Affichage
  create_thread_timer(); // Creation d'un thread qui gère les timeout
  create_orch_socket(); // Création du socket de l'Orchestrateur
  // Variables pour le calcul de temps
  time_t t;
  struct tm tm;
  // ----------
  // Buffer paquet "ok"
  char ok [16];
  strcpy(ok,"ok");
  // ----------
  char str_date[256]; // Buffer stoquage de dates
  int val; // Variable test retour des fonctions
  char * ip_str = NULL; // Variable pour free les chaînes de caractères
  // Code principal
  while (flag_stop==0){
    // Recherche si un paquet reste à lire dans le socket
    val = recvfrom(sockfd, buf_recv, 1024, 0, (struct sockaddr *)&addr_client, &my_addrlen);
    if(flag_stop) continue;
    if(val == -1){
        perror("error (recvfrom)");
        close(sockfd);
        exit(EXIT_FAILURE);
    }
   if(is_connect(buf_recv)){ // Si le paquet contient "connect"
      // Création et stockage du Noeud
      node_tmp = create_node(addr_client,buf_recv[7]);
      if(insert_node(node_tmp)==-1){
        // Suppression du Noeud créé prématurément pour l'insertion dans la liste
        free(node_tmp->read_time);
        free(node_tmp->cal_str);
        free(node_tmp->ip_str);
        free(node_tmp->cal);
        free(node_tmp);
        // Si le Noeud est déjà listé dans la liste, mise à 0 du timer et mise à jour de l'opérateur
        node_tmp = search_node(node_tmp->addr);
        pthread_mutex_lock(&list_mutex);
        node_tmp->operateur=buf_recv[7];
        pthread_mutex_unlock(&list_mutex);
        reset_timer(addr_client);
      }
      if (flag_debug){
        printf("\n\"connect\" paquet reçu de <%s,%d> , opérateur = %c\norchestrateur> ",ip_str=get_ip_string(node_tmp->addr),node_tmp->port,node_tmp->operateur);
        fflush(stdout);
        free_string(ip_str);
        ip_str=NULL;
      }
      printf("\nConnexion établie avec <%s,%d> , opérateur = %c\norchestrateur> ",ip_str=get_ip_string(node_tmp->addr),node_tmp->port,node_tmp->operateur);
      fflush(stdout);
      free_string(ip_str);
      ip_str=NULL;
      if (sendto(sockfd, ok, sizeof(ok) + 1, 0, (struct sockaddr *)&addr_client, sockaddr_length(addr_client)) == -1){
        perror("error (sendto)");
        close(sockfd);
        exit(EXIT_FAILURE);
      }
    }
    else if(is_hello(buf_recv)){ // Si le paquet contient "hello"
      if(reset_timer(addr_client)==-1){ // Mise à 0 du timer hello
        // Si reset_timer renvoie -1 -> Noeud non répertorié
        if (flag_debug){
          printf("\n\"hello\" paquet reçu d'un Noeud inconnu <%s,%d>\norchestrateur> ",ip_str=get_ip_string(addr_client),get_in_port(((struct sockaddr*)&addr_client)));
          fflush(stdout);
          free_string(ip_str);
          ip_str=NULL;
        }
      }
      else { // Sinon Noeud connu, envoi d'un paquet "ok"
        if(flag_debug){ // Affichage avec l'option debug
          node_tmp = search_node(addr_client);
          printf("\n\"hello\" paquet reçu de <%s,%d> , opérateur = %c\norchestrateur> ",ip_str=get_ip_string(node_tmp->addr),node_tmp->port,node_tmp->operateur);
          fflush(stdout);
          free_string(ip_str);
          ip_str=NULL;
        }
        if (sendto(sockfd, ok, sizeof(ok) + 1, 0, (struct sockaddr *)&addr_client, sockaddr_length(addr_client))== -1){
          perror("error (sendto)");
          close(sockfd);
          exit(EXIT_FAILURE);
        }
      }
    }
    // Si le paquet contient des chiffres c'est un résultat de calcul, affichage du résultat
    else if (isdigit(buf_recv[0])||((buf_recv[0]=='-')&&(isdigit(buf_recv[1])))){
      t = time(NULL);
      tm = *localtime(&t);
      strftime(str_date, sizeof(str_date),"%H:%M:%S", &tm);
      node_tmp=search_node(addr_client); // Recherche des informations du Noeud
      if (node_tmp==NULL){
        printf("Résultat d'un calcul inconnu : %s \norchestrateur> ",buf_recv);
      }
      else printf("\n%s %s = %s by <%s,%d> at %s\norchestrateur> ",node_tmp->read_time,node_tmp->cal_str,buf_recv,ip_str=get_ip_string(addr_client),node_tmp->port,str_date);
      fflush(stdout);
      free_string(ip_str);
      ip_str=NULL;
      // Remise en état opérationel du Noeud
      pthread_mutex_lock(&list_mutex);
      delete_cal(node_tmp->cal);
      node_tmp->timer_cal=0;
      node_tmp->timer_hello=0;
      node_tmp->occupied=0;
      pthread_mutex_unlock(&list_mutex);
      // ----------
    }
    else{
      printf("\nunknown socket message :%s \norchestrateur> ",buf_recv);
      fflush(stdout);
      sleep(1);
    }
  }
  return 0;
}
