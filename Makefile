CC = gcc
CCFLAGS = -Wall -g

all: orchestrateur noeud

orchestrateur: orchestrateur.c
	$(CC) $(CCFLAGS) -o orchestrateur orchestrateur.c -lm -pthread

noeud: noeuds.c
	$(CC) $(CCFLAGS) -o noeuds noeuds.c -lm -pthread

clean:
	rm orchestrateur noeuds
