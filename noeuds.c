#include "noeuds.h"
#define OK_TIMEOUT 35 // Limite du délai (sec) du dernier "ok" reçu avant reconnexion vers l'Orchestrateur
#define CONNECTION_TIMEOUT 5 // Limite du délai (sec) du mode connexion lors de l'attente du "ok"
#define ORCH_PORT 5000 // Port de l'orchestrateur

int sockfd; // Socket du Noeud
struct sockaddr_storage my_addr; // Adresse du Noeud
struct sockaddr_storage client; // Adresse de l'Orchestrateur
struct sockaddr_storage tmp_client; // Adresse tmp pour les paquets reçus
socklen_t my_addrlen; // Taille de l'adressage du Noeud
socklen_t client_addrlen; // Taille de l'adressage de l'Orchestrateur

int timer_ok; // Compteur des secondes écoulées après le dernier "ok" reçu
int timer_hello; // Compteur des secondes écoulées après le dernier "hello" envoyé
pthread_t pth; // Identifiant thread
char operateur; // Opérateur/Fonction du Noeud
int ip_type        = 0; // Type IPv du Noeud
int flag_debug     = 0; // Mode debug
int flag_broadcast = 0; // Mode broadcast

// Suppression des sauts de lignes dans la chaîne de caractères
// Retourne la chaine de caractère modifiée
char * delete_jumpline (char * str){
  int i;
  for (i=0;i<strlen(str);i++){
    if (str[i]=='\n') str[i]=' ';
  }
  return str;
}

// Retour du numéro de port
in_port_t get_in_port(struct sockaddr *sa){
    if (sa->sa_family == AF_INET) {
        return (((struct sockaddr_in*)sa)->sin_port);
    }
    return (((struct sockaddr_in6*)sa)->sin6_port);
}

// Retourne l'adresse IP en chaîne de caractères
char * get_ip_string(struct sockaddr_storage addr){
  char *s = NULL;
  switch(addr.ss_family) {
    case AF_INET: {
        struct sockaddr_in * addr_in = (struct sockaddr_in *)&addr;
        s = malloc(INET_ADDRSTRLEN);
        inet_ntop(AF_INET, &(addr_in->sin_addr), s, INET_ADDRSTRLEN);
        break;
    }
    case AF_INET6: {
        struct sockaddr_in6 * addr_in6 = (struct sockaddr_in6 *)&addr;
        s = malloc(INET6_ADDRSTRLEN);
        inet_ntop(AF_INET6, &(addr_in6->sin6_addr), s, INET6_ADDRSTRLEN);
        break;
    }
    default:
        break;
  }
  return s;
}

// Foction qui tronque la partie décimale pour les nombres entiers
// Retourne le résultat dans une chaîne de caractères
char * resultat_int_float(float x){
  char * buf = malloc (sizeof(char)*256);
  int point_position = 0; // Position de '.'
  int flag_decimal = 0; // Si chiffre significatif dans la partie décimale
  sprintf(buf,"%f",x);
  int i;
  for (i=0;(i<256)&&(buf[i]!='\0');i++){
    if (buf[i]=='.') { // Sauvegarde de la position de '.'
      point_position=i;
      continue;
    }
    if((point_position)&&(buf[i]>'0')&&(buf[i]<='9')){ // Parcours
      flag_decimal=1;
    }
  }
  if (!flag_decimal){ // Si pas de partie décimale, troncage
    buf[point_position]='\0';
  }
  return buf;
}

// Conversion du calcul en struct calcul
// Stoque le calcul dans la variable Calcul cal
// Retourne -1 si le calcul est non conforme et 0 sinon
int parser_calcul (char * string, Calcul cal){
  // Réinitialisation de la variable cal
  cal->operateur='\0';
  cal->x=0;
  cal->y=0;
  int xdec_pos          = 0; // Curseur de lecture de la partie décimale de x
  int ydec_pos          = 0; // Curseur de lecture de la partie décimale de y
  int indice            = 0; // Curseur de lecture du buffer string
  int etape             = 0; // Indice pour départager les différentes parties du string
  float tmp_val         = 0; // Variable temporelle pour les calculs
  int flag_x            = 0; // Test si x existe
  int flag_y            = 0; // Test si y existe
  int nb_parentheses    = 0; // Vérification des parentheses
  int flag_non_conforme = 0; // Vérification de la validité du calcul
  int flag_x_neg        = 0; // Si x est négatif
  int flag_y_neg        = 0; // Si y est négatif
  while ((indice < strlen(string))&&(etape < 3)){
    if (etape == 0){ // Lecture de l'opérateur
      cal->operateur = string[indice];
      etape ++;
      indice ++;
    }
    if (etape == 1){ // Lecture de x
      if(!isdigit(string[indice])){ // Test si c'est un chiffre
        if (string[indice]=='('){
          nb_parentheses++;
          indice ++;
          if(string[indice]=='-'){ // Si ',' lecture de y
            flag_x_neg=1;
            indice++;
          }
        }
        else if(string[indice]=='.'){ // Si '.' lecture de la partie decimale de x
          xdec_pos=1;
          indice++;
        }
        else if(string[indice]==','){ // Si ',' lecture de y
          etape++;
          indice++;
          if(string[indice]=='-'){ // Si ',' lecture de y
            flag_y_neg=1;
            indice++;
          }
        }
        else{
          flag_non_conforme = 1;
          break;
        }
      }
      else{
        if(xdec_pos>0){ // Partie décimale de x
          tmp_val=string[indice]-'0';
          cal->x=cal->x + (tmp_val/(pow(10,xdec_pos)));
          xdec_pos++;
          indice++;
        }
        else { // Partie entière de x
        flag_x=1;
        cal->x *= 10;
        cal->x += string[indice]-'0';
        indice++;
        }
      }
    }
    if (etape == 2){ // Lecture de y
      if(!isdigit(string[indice])){ // Test si c'est un chiffre
        if(string[indice]=='.'){ // Si '.' lecture de la partie decimale de y
          ydec_pos=1;
          indice++;
        }
        else if(string[indice]==')'){ // Si ')' fin de la lecture
          nb_parentheses++;
          etape++;
        }
        else{
          flag_non_conforme = 1;
          break;
        }
      }
      else{
        if(ydec_pos>0){ // Partie décimale de y
          tmp_val=string[indice]-'0';
          cal->y=cal->y + (tmp_val/(pow(10,ydec_pos)));
          ydec_pos++;
          indice++;
        }
        else { // Partie entière de y
        flag_y=1;
        cal->y *= 10;
        cal->y += string[indice]-'0';
        indice++;
        }
      }
    }
  }
  if (flag_x_neg) cal->x = -cal->x;
  if (flag_y_neg) cal->y = -cal->y;
  // Test de la validité du calcul
  if ((!flag_x)||(!flag_y)||(nb_parentheses!=2)||(flag_non_conforme)){
    printf("Calcul non conforme. Ex : +(3,7)\n");
    return -1; // Echec
  }
  // Test de la validité de l'opérateur
  if ((cal->operateur!='+')&&(cal->operateur!='-')&&(cal->operateur!='*')&&(cal->operateur!='/')){
    printf("Calcul non conforme. : opérateur non valide, veuillez choisir entre '+','-','/','*'\n");
    return -1; // Echec
  }
  // Test de la division par 0
  if ((cal->operateur=='/')&&(cal->y==0.)){
    printf("Calcul non conforme. : division par 0\n");
    return -1; // Echec
  }
  return 0; // Calcul avec succés
}

// Création de adressage du Noeud
// Si ip_type=4 alors l'adressage est en IPv4, sinon en IPv6
void create_node_socket(char * port, int ip_type){
  if(ip_type==4){ // Test type IPv
    // Création du socket Noeud IPv4
    if((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1){
        perror("error (socket)");
        exit(EXIT_FAILURE);
    }
    if (flag_debug) printf("Adressage IPv4.\n");
    // Initialisation de l'addresse du Noeud
    struct sockaddr_in my_addr2;
    memset (&my_addr2, 0, sizeof(my_addr2));
    my_addr2.sin_family      = AF_INET;
    my_addr2.sin_port        = htons (atoi(port));
    my_addrlen                  = sizeof(struct sockaddr_in);
    my_addr2.sin_addr.s_addr = htonl (INADDR_ANY);
    // Liaison entre adresse et socket
    if(bind(sockfd, (void *) &my_addr2, my_addrlen) == -1){
        perror("error (bind)");
        close(sockfd);
        exit(EXIT_FAILURE);
    }
    memcpy((char *)(&my_addr),(char *)(&my_addr2), sizeof(my_addr2));
  }
  else {
    // Création du socket Noeud IPv6
    if((sockfd = socket(AF_INET6, SOCK_DGRAM, 0)) == -1){
        perror("error (socket)");
        exit(EXIT_FAILURE);
    }
    if (flag_debug) printf("Adressage IPv6.\n");
    // Initialisation de l'addresse du Noeud
    struct sockaddr_in6 my_addr2;
    memset (&my_addr2, 0, sizeof(my_addr2));
    my_addr2.sin6_family    = AF_INET6;
    my_addr2.sin6_port      = htons (atoi(port));
    my_addrlen              = sizeof(struct sockaddr_in6);
    my_addr2.sin6_addr	    = in6addr_any;
    // Liaison entre adresse et socket
    if(bind(sockfd, (void *) &my_addr2, my_addrlen) == -1){
        perror("error (bind)");
        close(sockfd);
        exit(EXIT_FAILURE);
    }
    memcpy((char *)(&my_addr),(char *)(&my_addr2), sizeof(my_addr2));
  }
}

// Création de adressage de l'Orchestrateur
// Si ip_type=4 alors l'adressage est en IPv4, sinon en IPv6
void create_client_socket(int ip_type){
  if(ip_type==4){ // Test type IPv
    // Initialisation de l'addresse de l'Orchestrateur
    struct sockaddr_in client2;
    memset (&client2, 0, sizeof(client2));
    client2.sin_family      = AF_INET;
    client2.sin_port        = htons (ORCH_PORT);
    client_addrlen          = sizeof(struct sockaddr_in);
    // Traduction de l'adresse IP du client
    if(inet_pton(AF_INET, "127.0.0.1", &client2.sin_addr) != 1){
        perror("error (inet_pton)");
        close(sockfd);
        exit(EXIT_FAILURE);
    }
    memcpy((char *)(&client),(char *)(&client2), sizeof(client2)); // Sauvegarde
  }
  else {
    // Initialisation de l'addresse de l'Orchestrateur
    struct sockaddr_in6 client2;
    memset (&client2, 0, sizeof(client2));
    client2.sin6_family      = AF_INET6;
    client2.sin6_port        = htons (ORCH_PORT);
    client_addrlen           = sizeof(struct sockaddr_in6);
    // Traduction de l'adresse IP du client
    if(inet_pton(AF_INET6, "::1", &client2.sin6_addr) != 1){
        perror("error (inet_pton)");
        close(sockfd);
        exit(EXIT_FAILURE);
    }
    memcpy((char *)(&client),(char *)(&client2), sizeof(client2)); // Sauvegarde
  }
}

// Création d'un adresage de destination IPv4 Broadcast
void create_broadcast_client_socket(){
  // Création du socket client
  if((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1){
    perror("error (socket)");
    exit(EXIT_FAILURE);
  }
  // Option broadcast
  int broadcastEnable=1;
  if (setsockopt(sockfd, SOL_SOCKET, SO_BROADCAST, &broadcastEnable, sizeof(broadcastEnable))){
    perror("error (setsockopt)");
    close(sockfd);
    return;
  }
  if (flag_debug)printf("Adressage client Broadcast.\n"); // Affichage avec l'option debug
  // Initialisation de la structure d'adressage
  struct sockaddr_in client2;
  memset (&client2, 0, sizeof(client2));
  client2.sin_family      = AF_INET;
  client2.sin_port        = (in_port_t)htons(ORCH_PORT);
  client2.sin_addr.s_addr = htonl(INADDR_BROADCAST);
  client_addrlen           = sizeof(struct sockaddr_in);
  memcpy((char *)(&client),(char *)(&client2), sizeof(client2));
}

// Fonction de connexion avec l'Orchestrateur
// Envoi de paquet "connect" et attente d'un paquet "ok" pendant CONNECTION_TIMEOUT secondes
// Retourne 0 si succés et -1 sinon
int connect_to_client(char operateur) {
  int sel_val=0; // Retour de Select
  int i; // Indice pour boucle
  int counter_connect=0; // Comptage jusqu'a CONNECTION_TIMEOUT
  // Buffer pour envoi du message connect
  char buf [1024];
  strcpy(buf,"connect");
  buf[7]=operateur;
  // Variables pour Select
  struct timeval tv;
  tv.tv_sec = 0; // Timout 0 pour Select
  tv.tv_usec = 0; // Timout 0 pour Select
  fd_set readfds;
  fd_set saved_readfds;
  FD_ZERO(&readfds);
  FD_ZERO(&saved_readfds);
  FD_SET(sockfd, &readfds); // Inclusion du socket
  saved_readfds=readfds; // Création d'une sauvegarde
  // Envoi d'un paquet "connect" à l'Orchestrateur
  if (sendto(sockfd, buf, 1024, 0, (struct sockaddr *)&client, client_addrlen)== -1){
    perror(" error (sendto)");
    close(sockfd);
    exit(EXIT_FAILURE);
  }
  // Attente d'un paquet "ok" pendant CONNECTION_TIMEOUT secondes
  while((sel_val==0)&&(counter_connect<CONNECTION_TIMEOUT)){
    sel_val = select(sockfd + 1, &readfds, NULL, NULL, &tv);
    readfds=saved_readfds;
    if (sel_val==-1){
      perror("error (select)");
      close(sockfd);
      exit(EXIT_FAILURE);
    }
    for(i=0;i<sel_val;i++){ // Si paquets reçus, lecture en espérant de trouver une réponse "ok"
      if (recvfrom(sockfd, buf, 1024, 0, (struct sockaddr *)&tmp_client, &my_addrlen)== -1){
          perror("error (recvfrom)");
          close(sockfd);
          exit(EXIT_FAILURE);
      }
      if ((buf[0]=='o')&&(buf[1]=='k')){
        printf("Connexion avec l'Orchestrateur réussie.\n");
        // client=tmp_client; // Sauvegarde de l'adresse du client
        return 0; // Succés
      }
      else {
        if(flag_debug)printf("Paquet inconnu reçu : %s\n",buf); // Affichage avec l'option debug
        return -1; // Echec
      }
    }
    sleep(1);
    counter_connect++;
  }
  return -1;
}

// Fonction de connexion avec l'Orchestrateur
// Envoi de paquet "connect" et attente d'un paquet "ok" pendant CONNECTION_TIMEOUT secondes
// Utilisé aprés création des thread pour la récupération de la connexion
// Lecture du paquet "ok" se déroule dans un autre thread (main)
// Retourne -1 en cas d'échec et 0 sinon
int reconnect_to_client(char operateur) {
  printf("Essai de reconnexion.\n");
  char buf [1024];
  strcpy(buf,"connect");
  buf[7]=operateur;
  int timer_connect=0;
  if (sendto(sockfd, buf, 1024, 0, (struct sockaddr *)&client, client_addrlen)== -1){
    perror(" error (sendto)");
    close(sockfd);
    exit(EXIT_FAILURE);
  }
  while(timer_connect<CONNECTION_TIMEOUT){
    sleep(1);
    timer_connect++;
    if (timer_ok<OK_TIMEOUT) break;
  }
  if(timer_ok>OK_TIMEOUT){
    printf("Reconnexion échouée.\n");
    return -1;
  }
  printf("Connexion avec l'Orchestrateur réussie.\n");
  return 0;
}

// Fonction thread qui gère la connexion avec l'Orchestrateur
// Envoie un paquet "hello" toutes les 10 secondes
// Rentre en mode reconnexion si OK_TIMEOUT atteint, exit en cas d'échec
void * thread_timer_control (){
  char hello[24]; // Buffer
  while(1){
    // Affichage avec l'option debug
    if (flag_debug)printf("timer_hello %d timer_ok %d.\n",timer_hello,timer_ok);
    fflush(stdout);
    sleep(1);
    timer_ok++;
    timer_hello++;
    // Si on ne reçoit pas de réponse "ok" aux paquets "hello" au bout de OK_TIMEOUT sec
    // On réessaye la phase de connexion jusqu'à reçevoir une réponse de l'Orchestrateur
    if (timer_ok>OK_TIMEOUT){
      printf("Connexion perdue.\n");
      if(reconnect_to_client(operateur)==-1){ // Recherche de connexion
        exit(0); // Si échec, fermeture du Noeud
      }
      timer_ok=0; // Si succés, remise à 0 du compteur_ok
    }
    // Envoi de paquets "hello" toutes les 10 sec
    if(timer_hello>=10){
      strcpy(hello,"hello");
      if (sendto(sockfd, hello, strlen(hello), 0, (struct sockaddr *)&client, client_addrlen)== -1){
        perror("error (sendto)");
        close(sockfd);
        exit(EXIT_FAILURE);
      }
      if (flag_debug){ // Affichage avec l'option debug
        char * ip_str;
        printf("Envoi \"hello\" à l'Orchestrateur <%s,%d>.\n",ip_str=get_ip_string(client),get_in_port((struct sockaddr*)&client));
        fflush(stdout);
        free(ip_str);
      }
      timer_hello=0;
    }
  }
}

// Création du threat qui gère la connexion
void create_thread_timer(){
  if(pthread_create(&pth, NULL, thread_timer_control, NULL)!=0){
    perror("phthread_create error");
  }
}

// Fonction qui vérifie les arguuments du main
// Si échec exit du processus
void verify_arguments(int argc, char ** argv){
  // Récupération des options dans le Main
  int get_opt; // Retour getopt
  int nb_opt_arguments=0; // Nombre d'arguments dédiées aux options
  int i; // Indice boucle for
  while ((get_opt = getopt (argc, argv, "db"))!=-1){ // Recherche des options avec getopt
      switch (get_opt){
        case 'd': // Option debug
          flag_debug = 1;
          nb_opt_arguments++;
          break;
        case 'b': // Option broadcast
          flag_broadcast = 1;
          nb_opt_arguments++;
          break;
        case '?':
          fprintf(stderr, "Option -%c inconnue.\n",optopt);
          exit(1);
          break;
        default:
          fprintf (stderr, "error getopt\n");
          exit(1);
      }
  }
  if ((flag_debug)&&(flag_broadcast)){
    if((strlen(argv[1])-1)==2) nb_opt_arguments = 1;
    else nb_opt_arguments = 2;
  }
  // Test du nombre d'arguments entrés
  if(argc != (4+nb_opt_arguments)){
      fprintf(stderr,"Utilisation: %s [-d] [-b] ip_type local_port operateur \nExemple : %s 4 5001 '+'\n", argv[0], argv[0]);
      exit(1);
  }
  // Test validité du type IPv
  if (!flag_broadcast){
    if (((atoi(argv[argc-3])!=4)&&(atoi(argv[argc-3])!=6))||(strlen(argv[argc-3])!=1)){
      fprintf(stderr,"Utilisation: %s [-d] [-b] ip_type local_port operateur \n Choisissez un type ip valide : '4','6'\n", argv[0]);
      exit(1);
    }
  }
  // Test validité du port, s'il ne contient que des chiffres
  for (i=0; i<strlen(argv[argc-2]);i++){
    if (!isdigit(argv[argc-2][i])){
      fprintf(stderr,"Utilisation: %s [-d] [-b] ip_type local_port operateur \n Choisissez un port valide\n", argv[0]);
      exit(1);
    }
  }
  // Test validité de l'opérateur/fonction
  for (i=0; i<strlen(argv[argc-1]);i++);
  if ((i>1)||((argv[argc-1][0]!='/')&&(argv[argc-1][0]!='+')&&(argv[argc-1][0]!='-')&&(argv[argc-1][0]!='*'))){
    fprintf(stderr,"Utilisation: %s [-d] [-b] ip_type local_port operateur \n Choisissez un opérateur valide : '+','-','/','*'\n", argv[0]);
    exit(1);
  }
}

//Fonction de création d'un calcul
//Retourne : structu calcul
Calcul create_cal (){
  Calcul cal = malloc (sizeof (struct calcul));
  cal->x=0;
  cal->y=0;
  cal->operateur='\0';
  return cal;
}

// Fonction main :
// Initialisation des variables -> création du thread
// -> lecture des paquets rentrants et calcul + envoi du résultat juqu'à la fin du processus
int main(int argc, char **argv){
    verify_arguments(argc,argv);
    // Variables variées
    char buf[1024]; // Buffer
    memset(buf,'\0',1024); // Initialisation du buffer
    char * tmp_buf; // Pointeur pour les messages
    ip_type        = argv[argc-3][0]-'0'; // Type ipv4 ou ipv6
    operateur      = argv[argc-1][0]; // Opérateur/Fonction du processus
    timer_ok       = 0; // Compteur des secondes écoulées après le dernier "ok" reçu
    timer_hello    = 0; // Compteur des secondes écoulées après le dernier "hello" envoyé
    float result   = 0; // Variable résultat de calcul
    int flag_ready = 0; // Boolean si calcul est bon pour l'envoi
    Calcul cal     = create_cal(); // Variable de stockage des calculs
    // ----------
    // Création de l'adresse Noeud en fonction du type de IP
    create_node_socket(argv[argc-2],ip_type);
    if(flag_broadcast){
      // Création de l'adresse de l'Orchestrateur soit en Broadcast
      create_broadcast_client_socket();
    }
    else{
      // Création de l'adresse de l'Orchestrateur du même type IP que l'adresse du Noeud
      create_client_socket(ip_type);
    }
    // Connexion avec l'Orchestrateur
    if(flag_broadcast) printf("Essai de connexion en Broadcast.\n");
    else printf("Essai de connexion en IPv%d.\n",ip_type);

    if (connect_to_client(operateur)==-1){
          printf("Connexion échouée.\n");
          exit(0);
    }
    // ----------
    create_thread_timer(); // Création du thread de gestion des timeout
    while(1){
      if (recvfrom(sockfd, buf, 1024, 0, (struct sockaddr *)&tmp_client, &my_addrlen)== -1){
        perror("error (recvfrom)");
        close(sockfd);
        exit(EXIT_FAILURE);
      }
      // Si paquet "ok" reçu, mise à zéro du compteur timer_ok
      if ((buf[0]=='o')&&(buf[1]=='k')){
        if(flag_debug){ // Affichage avec l'option debug
          char * ip_str;
          printf("\"ok\" paquet reçu de l'Orchestrateur <%s,%d>.\n",ip_str=get_ip_string(my_addr),get_in_port((struct sockaddr*)&my_addr));
          fflush(stdout);
          free(ip_str);
        }
        timer_ok=0;
      }
      else if (parser_calcul(buf,cal)!=-1){ // Test si c'est un calcul conforme et stoque dans cal si oui
        // Recherche de la fonction correspondante au Noeud
        // Test si la fonction du Noeud est adaptée au calcul reçu dans le buffer
        if ((cal->operateur=='+')&&(operateur=='+')){
          result = cal->x + cal->y;
          printf("Calcul reçu : %s\n",delete_jumpline(buf));
          tmp_buf=resultat_int_float(result);
          printf("Envoi du résultat : %s\n",tmp_buf);
          flag_ready=1;
        }
        else if ((cal->operateur=='-')&&(operateur=='-')){
          result = cal->x - cal->y;
          printf("Calcul reçu : %s\n",delete_jumpline(buf));
          tmp_buf=resultat_int_float(result);
          printf("Envoi du résultat : %s\n",tmp_buf);
          flag_ready=1;
        }
        else if ((cal->operateur=='*')&&(operateur=='*')){
          result = cal->x * cal->y;
          printf("Calcul reçu : %s\n",delete_jumpline(buf));
          tmp_buf=resultat_int_float(result);
          printf("Envoi du résultat : %s\n",tmp_buf);
          flag_ready=1;
        }
        else if ((cal->operateur=='/')&&(operateur=='/')){
          result = cal->x / cal->y;
          printf("Calcul reçu : %s\n",delete_jumpline(buf));
          tmp_buf=resultat_int_float(result);
          printf("Envoi du résultat : %s\n",tmp_buf);
          flag_ready=1;
        }
        else{
          // Si échec, calcul non approprié au Noeud
          printf("Erreur : calcul non approprié\n");
        }
        if (flag_ready){ // Si le calcul à été résolu, envoi du résultat à l'Orchestrateur
          if (sendto(sockfd, tmp_buf, 1024, 0, (struct sockaddr *)&client, client_addrlen) == -1){
            perror("error (sendto)");
            close(sockfd);
            exit(EXIT_FAILURE);
          }
        }
      }
      else{
        if (flag_debug) printf("Paquet invalide reçu.\n"); // Affichage avec l'option debug
      }
    }
  return 0;
}
