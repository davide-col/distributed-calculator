#ifndef NOEUDS_H
#define NOEUDS_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>
#include <math.h>

// Structure d'un calcul
typedef struct calcul {
  char operateur; // Opérateur
  float x; // Valeur x
  float y; // Valeur y
  int exists; // Boolean à 1 si calcul non traité et 0 sinon
} * Calcul;

// Suppression des sauts de lignes dans la chaîne de caractères
// Retourne la chaine de caractère modifiée
char * delete_jumpline (char *);

// Retour du numéro de port
in_port_t get_in_port(struct sockaddr *);

// Retourne l'adresse IP en chaîne de caractères
char * get_ip_string(struct sockaddr_storage);

// Foction qui tronque la partie décimale pour les nombres entiers
// Retourne le résultat dans une chaîne de caractères
char * resultat_int_float(float );

// Conversion du calcul en struct calcul
// Stoque le calcul dans la variable Calcul cal
// Retourne -1 si le calcul est non conforme et 0 sinon
int parser_calcul (char *, Calcul);

// Création de adressage du Noeud
// Si ip_type=4 alors l'adressage est en IPv4, sinon en IPv6
void create_node_socket(char *, int);

// Création de adressage de l'Orchestrateur
// Si ip_type=4 alors l'adressage est en IPv4, sinon en IPv6
void create_client_socket(int);

// Création d'un adresage de destination IPv4 Broadcast
void create_broadcast_client_socket();

// Fonction de connexion avec l'Orchestrateur
// Envoi de paquet "connect" et attente d'un paquet "ok" pendant CONNECTION_TIMEOUT secondes
// Retourne 0 si succés et -1 sinon
int connect_to_client(char);

// Fonction de connexion avec l'Orchestrateur
// Envoi de paquet "connect" et attente d'un paquet "ok" pendant CONNECTION_TIMEOUT secondes
// Utilisé aprés création des thread pour la récupération de la connexion
// Lecture du paquet "ok" se déroule dans un autre thread (main)
// Retourne -1 en cas d'échec et 0 sinon
int reconnect_to_client(char);

// Fonction thread qui gère la connexion avec l'Orchestrateur
// Envoie un paquet "hello" toutes les 10 secondes
// Rentre en mode reconnexion si OK_TIMEOUT atteint, exit en cas d'échec
void * thread_timer_control ();

// Création du threat qui gère la connexion
void create_thread_timer();

// Fonction qui vérifie les arguuments du main
// Si échec exit du processus
void verify_arguments(int, char **);

//Fonction de création d'un calcul
//Retourne : structu calcul
Calcul create_cal ();

#endif
