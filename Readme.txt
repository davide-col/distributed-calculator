Compilation :
 	$ make

Exécution des Noeuds:
 	$ ./noeuds  [-d] [–b] ip_type   local_port  operateur
Exemples :
$ ./noeuds -d  4  5002 ‘+’
$ ./noeuds  -b 6  5004 ‘–‘

Exécution de Orchestrateur:
	 $ ./orchestrateur [-d]

Options :
•	-b : Option « mode broadcast » utilisé uniquement par le programme Nœud qui permet de faire une annonce 
  broadcast à l’Orchestrateur afin de ne pas avoir besoin de l’adresse IP de l’orchestrateur.
•	 -d : Option « mode debug » valable pour l’Orchestrateur et les Nœuds permettant d’affiner l’affichage des processus et protocoles en arrière plan.
  Les options viennent obligatoirement en première position dans les arguments du main. Possibilité de les regrouper (-db).

Calculs supportés (Exemples):
•	orchestrateur> +(5.0,-9.3)  : addition
•	orchestrateur> -(5,4.0)    : soustraction
•	orchestrateur> *(1,-2)  : multiplication
•	orchestrateur> /(1,5)    : division
Compatible avec les valeurs négatives, parties décimales et vérification de la division par 0.

Commandes supplémentaires (Orchestrateur) :
•	orchestrateur> show : affichage des nœuds présents dans la liste (connectés).
•	orchestrateur> exit    : arrêt de l’orchestrateur.
